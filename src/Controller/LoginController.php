<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;       //Abstract Class
use Symfony\Component\HttpFoundation\Response;                          //Responses
use Symfony\Component\Routing\Annotation\Route;                         //Routes
use Symfony\Component\HttpFoundation\RequestStack;                      //Sessions
use Symfony\Component\HttpFoundation\Cookie;                            //Cookies
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

//Requests

class LoginController extends AbstractController
{

    #[Route('/login', name: 'app_login')]
    public function index(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        // Erro que ocorreu durante o login do usuario
        $error = $authenticationUtils->getLastAuthenticationError();

        // Ultimo usuario digitado
        $lastUsername = $authenticationUtils->getLastUsername();

        // Exibe os dados do View
        return $this->render('login/index.html.twig', [
            'error' => $error,
            'last_username' => $lastUsername,
        ]);
    }
}
